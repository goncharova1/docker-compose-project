# Docker Compose Project

Проект создан для демонстрации работы контейнеров через Docker Compose. 
Учебный проект. 
Чтобы запустить контейнеры введите: 

docker compose up 

Чтобы посмотреть запущенные контейнеры 

docker compose ps

Чтобы остановить контейнеры:

docker compose stop